# Changelog

All notable changes to the project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.4.1]

- #4 fixed - Better method of checking that fm azimuth bounds don't exceed server azimuth bounds
- #5 fixed - Don't include filtered rays in a frame

## [0.4.0]

- Implemented many tests in pytest for FMConfig
- Fixed invalid checks of azimuth and range config values based on tests

## [0.3.4] - 2021-03-04

- hpx-messages 1.0.0
- hpx-server-monitor 0.2.3

## [0.3.3] - 2021-02-10

- hpx-messages 0.2.0

## [0.3.2] - 2021-02-04

- Dataclass fields set in __post_init__ don't need to be Optional.
- Use hpx-server-monitor 0.2.1

## [0.3.1] - 2020-12-11

- Use hpx-server-monitor 0.2.0

## [0.3.0] - 2020-11-23

### Modifications

- Require latest hpx-server-monitor and hpx-messages
- new-frame and full-frame logic reworked so frame sized by number of summed rays works, as well as by
  azimuths.
- Use simple-version package to get the version rather than my own module. Mine wasn't working right.
- FMConfig fields can no longer be updated. Instead generate a new copy with updated fields using
  FMConfig.replace()

## [0.2.6] - 2020-11-02

- Use haller pypi for hpx-server-monitor and hpx-messages

## [0.2.5] - 2020-10-22

- Perform smoothing on unwrapped azimuth

## [0.2.4] - 2020-10-23

- Added azi_smoothed to Frame to eliminate duplicate azimuths

## [0.2.3] - 2020-10-20

- Set HWM to be the max size of the frame buffer
- Add haller pypi upload script

## [0.2.2] - 2020-10-19

- Use zmq-quicklink
- Use RCVTIMEO instead of a manual sleep after an empty recv

## [0.2.1] - 2020-10-07

### Added

- CHANGELOG.md
- frame_producer.py (split off from frame_maker.py)

### Modified

- Get version from package version indirectly so `pyproject.toml` is the only home of the version
- Docstrings and minor refactoring for code readability. No major functional changes.

## [0.2.0] -  2020-09-25

First release.

[Unreleased]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-framemaker/-/compare/v0.4.1...master
[0.4.1]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-framemaker/-/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-framemaker/-/compare/v0.3.4...v0.4.0
[0.3.4]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-framemaker/-/compare/v0.3.3...v0.3.4
[0.3.3]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-framemaker/-/compare/v0.3.2...v0.3.3
[0.3.2]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-framemaker/-/compare/v0.3.1...v0.3.2
[0.3.1]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-framemaker/-/compare/v0.3.0...v0.3.1
[0.2.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-framemaker/-/tags/v0.3.0
