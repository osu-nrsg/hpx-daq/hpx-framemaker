from pathlib import Path

import toml

from hpx_framemaker import __version__


def test_version():
    ppt_path = Path(__file__).parent.parent / "pyproject.toml"
    config = toml.load(ppt_path)
    assert config["tool"]["poetry"]["version"] == __version__
