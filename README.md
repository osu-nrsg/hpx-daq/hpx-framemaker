# HPx Framemaker README

## Introduction

The HPx Frame Maker is a Python (3.8+) library and a component of the HPx-DAQ project from the
Nearshore Remote Sensing Group at Oregon State University.

The role of the frame maker is to receive rays from an
[HPx Radar Server](https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-server), compile the ray data into frames
(whole or partial radar sweeps) and provide frames to downstream processing such as display or recording
applications.

## Example Usage

```python
from hpx_framemaker import FrameMaker, FMConfig

# Create the config. Numerous additional args available to constrain azimuth and range coverage.
# These are checked using an HPx ServerMonitor instance at this time so the server must be running.
fmc = FMConfig("ipc:///run/hpx-radar-server/hpx-pub.sock")

# Create the FrameMaker.
# multiproc=False runs in a separate thread
# multiproc=True runs in a separate process using shared memory
fm = FrameMaker(fmc, multiproc=False)
# Start the FrameMaker. This starts the separate thread or process that gets the Frames.
# Alternatively, use  `start` and `stop` methods.
with fm:
    # Some kind of loop to limit the duration of acquiring frames
    for _ in range(64):
        # Use the `next_frame()` context manager to acquire a lock on the next ready frame and return the frame
        with fm.next_frame() as frame:
            data = frame.data  # 2D intensity (azi, rg)
            azivals = frame.azi  # 1D ray acp count values
            time = frame.time  # 1D ray timestamps
            rg = fm.rg  # range bin locations in meters (constant for life of FrameMaker).
            # ...process/display/record frame data...
            # Each frame needs to be processed at least as fast as frames are coming in, or frames will be
            # dropped
        # After this `with` block the frame is no longer accessible
# FrameMaker is shutdown
```

## Notes

- Keyword arguments for FrameMaker include `start_azi`, `end_azi`, `srays_per_frame`, `min_range`,
  `max_range`, and `num_samples`.
  - If any of these constraints fall outside the bounds of the server configuration, and error is raised.
  - `start_azi` and `end_azi` are specified in degrees.
  - `start_azi` defaults to 0, `end_azi` defaults to 360 degree coverage.
  - `end_azi` and `srays_per_frame` (summed rays per frame) are mutually exclusive.
  - `min_range` and `max_range` are specified in meters.
  - `max_range` and `num_samples` are mutually exclusive. 
- The FrameMaker does briefly use an instance of
  [HPx Server Monitor](https://gitlab.com/osu-nrsg/hpx-daq/hpx-server-monitor-python) when it starts up to
  retrieve some settings from the HPx server which will raise a TimeoutError if the server does not appear to
  be running. However it does not maintain any monitoring of the HPx server and this should be done from
  calling code with a separate instance of HPx Server Monitor.
