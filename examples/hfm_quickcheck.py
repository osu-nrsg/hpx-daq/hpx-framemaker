import sys
import time
from contextlib import ExitStack

import numpy as np
from hpx_server_monitor import ServerMonitor

sys.path.append("../")
from hpx_framemaker import FMConfig, FrameMaker

# endpoint = "ipc:///run/hpx-radar-server/hpx-pub.sock"
endpoint = "tcp://10.5.142.11:10001"
fmc = FMConfig(endpoint, srays_per_frame=370)

print("Receive 10 frames using srays_per_frame as the end criterion")
with ExitStack() as estack:
    fm: FrameMaker = estack.enter_context(FrameMaker(fmc, multiproc=True))
    servmon: ServerMonitor = estack.enter_context(ServerMonitor(endpoint, 2))
    servmon.wait_status_config()
    last_t = None
    for _ in range(10):
        servmon.check_and_raise()
        frame_timeout_s = 4
        with fm.next_frame(frame_timeout_s) as frame:
            if not frame:
                raise TimeoutError(f"No frame received in {frame_timeout_s} seconds.")
            t = frame.time[-1]
            if last_t:
                print((t - last_t) / np.timedelta64(1, "s"))
            last_t = t
            time.sleep(1.2)
print("Done.")

fmc = fmc.replace(srays_per_frame=None, end_azi=190.0)
print("Receive 10 frames using end_azi as the end criterion")
with ExitStack() as estack:
    fm: FrameMaker = estack.enter_context(FrameMaker(fmc, multiproc=True))
    servmon: ServerMonitor = estack.enter_context(ServerMonitor(endpoint, 2))
    servmon.wait_status_config()
    last_t = None
    for _ in range(10):
        servmon.check_and_raise()
        frame_timeout_s = 4
        with fm.next_frame(frame_timeout_s) as frame:
            if not frame:
                raise TimeoutError(f"No frame received in {frame_timeout_s} seconds.")
            t = frame.time[-1]
            if last_t:
                print((t - last_t) / np.timedelta64(1, "s"))
            last_t = t
            time.sleep(1.2)
print("Done")
