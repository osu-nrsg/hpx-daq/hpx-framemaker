"""
Classes for radar frames.

Frame is for serial use. Choose FrameTD or FrameMP for threading or multiprocessing, respectively.
"""
import _thread
import ctypes
import logging
import multiprocessing.synchronize
import threading
from dataclasses import dataclass
from multiprocessing.managers import SharedMemoryManager
from multiprocessing.shared_memory import SharedMemory
from typing import Any, Optional, Tuple, Union

import numpy as np
from hpx_messages import RayMsg

from .errors import FullFrame
from .timed_lock import TimedLock

logger = logging.getLogger(__name__)

LockType = Union[_thread.LockType, multiprocessing.synchronize.Lock]


@dataclass
class FrameSyncArgs:
    """Structure for creating a Frame object in another process that shares memory with an existing frame."""

    data_shm: SharedMemory
    azi_shm: SharedMemory
    time_shm: SharedMemory
    nrays_val: Any  # cannot annotate multiprocessing.sharedctypes for some reason
    lock: multiprocessing.Lock

    def __getitem__(self, item):
        """Allow for star-unpacking"""
        return tuple(self.__dict__.values())[item]


FrameShapeArgs = Tuple[int, int, slice]
SharedFrameArgs = Tuple[FrameShapeArgs, FrameSyncArgs]


class Frame:
    """Abstraction of a [portion of] a single sweep of radar data.

    A frame is preallocated to a certain maximum size but with a virtual size of 0. As rays are added to the
    frame, the virtual size increases until the maximum size (at which point a `FullFrame` exception is
    raised if more rays are added). Each of the `data`, `azimuth`, and `time` properties returns the portion
    of the preallocated buffer for that frame attribute determined by the virtual size. When the frame is
    reset, the virtual size is set back to 0.

    The frame holds a lock object (from either `threading` or `multiprocessing`) that is acquired for adding
    rays or resetting the frame. Users of the frame should acquire the lock while accessing the frame's data.

    The frame's memory may be allocated using a `SharedMemoryManager` from `multiprocessing.managers` or use
    existing shared memory from another `Frame` object by supplying a `FrameSyncArgs` object.
    (See documentation for `Frame.__init__`)
    """

    def __init__(
        self,
        max_nrays: int,
        nsamples: int,
        rg_slice: slice,
        *,
        shared_mem_mgr: Optional[SharedMemoryManager] = None,
        frame_sync_args: Optional[FrameSyncArgs] = None,
    ):
        """Set up a Frame instance and preallocate space for the frame data.

        Parameters
        ----------
        max_nrays
            Number of rays to preallocate into the frame. The frame cannot grow larger than this, so make
            it conservatively big.
        nsamples
            Number of samples in each ray. This cannot change for the lifetime of the Frame.
        rg_slice
            Slice object to apply to the range dimension of the ray data before storing in the fraame.
        shared_mem_mgr : optional
            SharedMemoryManager to use to create the shared memory for the numpy arrays. Do not also specify
            `frame_sync_args`.
        frame_sync_args : optional
            FrameSyncArgs consisting of SharedMemory and Lock objects from another FrameMP to be used with
            this Frame. Do not also specify `shared_mem_mgr`.

        Raises
        ------
        ValueError
            Both `shared_mem_mgr` and `frame_args` are specified.

        """
        self._max_nrays: int = max_nrays
        self._nsamples: int = nsamples
        self._rg_slice: slice = rg_slice
        self._frame_sync_args: Optional[FrameSyncArgs] = None

        if shared_mem_mgr and frame_sync_args:
            raise ValueError("Specify at most one of `shared_mem_mgr` or `frame_sync_args`.")
        # set up shared memory and lock depending on if this is used with threading or multiprocessing
        if shared_mem_mgr or frame_sync_args:
            # multiprocessing
            if shared_mem_mgr:
                # This frame owns the shared memory objects. Create shared memory.
                data_shm = shared_mem_mgr.SharedMemory(size=max_nrays * nsamples * 2)  # uint16: 2
                azi_shm = shared_mem_mgr.SharedMemory(size=max_nrays * 2)  # uint16: 2
                time_shm = shared_mem_mgr.SharedMemory(size=max_nrays * 8)  # datetime64: 8
                nrays_val = multiprocessing.Value("L")
                lock = multiprocessing.Lock()
                self._frame_sync_args = FrameSyncArgs(data_shm, azi_shm, time_shm, nrays_val, lock)
            else:
                # This frame mirrors another frame that has created the shared memory.
                self._frame_sync_args = frame_sync_args
            databuf = self._frame_sync_args.data_shm.buf
            azibuf = self._frame_sync_args.azi_shm.buf
            timebuf = self._frame_sync_args.time_shm.buf
            nrays_val = self._frame_sync_args.nrays_val
            lock = self._frame_sync_args.lock
        else:
            # threading, so no shared memory
            databuf = None
            azibuf = None
            timebuf = None
            nrays_val = ctypes.c_ulong()
            lock = threading.Lock()

        # Set up the data storage arrays, using shared memory buffers if specified
        self._data = np.ndarray(shape=(max_nrays, nsamples), dtype=np.uint16, buffer=databuf)
        self._azi = np.ndarray(shape=(max_nrays,), dtype=np.uint16, buffer=azibuf)
        self._time = np.ndarray(shape=(max_nrays,), dtype="datetime64[ns]", buffer=timebuf)
        self._nrays_ = nrays_val  # This is a ctypes or shared memory value (for consistent interface)
        self._nrays = 0  # set _nrays_ with the _nrays setter
        self.lock: LockType = lock

    @property
    def shared_frame_args(self) -> SharedFrameArgs:
        """Args to set up companion frames with shared memory and lock objects.

         When the SharedMemoryManager used in creating this frame is shutdown, the shared memory objects
         will be invalid.
         """
        if self._frame_sync_args:
            return (self._max_nrays, self._nsamples, self._rg_slice), self._frame_sync_args

    @property
    def max_nrays(self) -> int:
        return self._max_nrays

    @property
    def nrays(self) -> int:
        return self._nrays

    @property
    def _nrays(self) -> int:
        """Internal getter for ctypes object"""
        return int(self._nrays_.value)

    @_nrays.setter
    def _nrays(self, val: np.uint16):
        """Internal setter for ctypes object"""
        self._nrays_.value = val

    @property
    def data(self) -> np.ndarray:
        """The radar intensity data of the frame [np.uint16]"""
        return self._data[: self._nrays, :]

    @property
    def azival(self) -> np.ndarray:
        """The ACP count values for each ray of the frame. [np.uint16]"""
        return self._azi[: self._nrays]

    @property
    def azi(self) -> np.ndarray:
        """Azimuth angles in degrees for each ray of the frame. [np.float32]"""
        return np.float32(360) * self.azival / 65536

    @property
    def time(self) -> np.ndarray:
        """The timestamps for each ray of the frame. [np.datetime64]"""
        return self._time[: self._nrays]

    @property
    def azi_smoothed(self) -> np.ndarray:
        """Azimuth angles in degrees with duplicates removed by smoothing"""
        azi_to_smooth = np.unwrap(self.azi * np.pi / 180.0)  # unwrapped radians
        smoothed_azi = azi_to_smooth.copy()
        # Get the index of the first of each pair of duplicates
        dup_azi_i = np.where(np.diff(azi_to_smooth) == 0)[0]
        for i, k in zip(dup_azi_i[:-1], dup_azi_i[1:]):
            # Find the slope between pairs of duplicates and use that to recreate the azimuths inbetween
            slope = (azi_to_smooth[k] - azi_to_smooth[i]) / (k - i)
            smoothed_azi[i:k] = azi_to_smooth[i] + np.arange(k - i) * slope
        return (smoothed_azi * 180.0 / np.pi) % 360.0  # wrapped degrees

    def reset(self):
        """Reset the ray to empty.

        Raises
        ------
        LockHeld
            The lock could not be acquired (e.g. a consumer is holding the lock).
        """
        with TimedLock(self.lock, 0):
            self._nrays = 0

    def add_ray(self, ray_msg: RayMsg):
        """Add the data to the frame, growing the frame if necessary.

        Parameters
        ----------
        ray_msg
            HPx ray message.

        Raises
        ------
        FullFrame
            The the frame is full and can take no more rays.
        LockHeld
            The lock could not be acquired (e.g. a consumer is holding the lock).
        """
        with TimedLock(self.lock, 0):
            if self._nrays >= self._data.shape[0]:
                raise FullFrame
            ray_i = self._nrays
            self._data[ray_i, :] = ray_msg.data[self._rg_slice]
            self._azi[ray_i] = ray_msg.azimuth
            self._time[ray_i] = ray_msg.calc_time
            self._nrays += 1
