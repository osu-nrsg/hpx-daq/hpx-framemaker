from __future__ import annotations

import _thread
import multiprocessing.synchronize
from typing import Union

from .errors import LockHeld, LockTimeout

LockType = Union[_thread.LockType, multiprocessing.synchronize.Lock]


class TimedLock:
    """Simple wrapper around a lock to enforce a timeout and have a context manager"""

    def __init__(self, lock: LockType, timeout: float):
        """

        Parameters
        ----------
        lock : multiprocessing.Lock or threading.Lock
            Lock instance
        timeout
            timeout to enforce (in seconds)
        """
        self._lock = lock
        self._timeout = timeout

    def __enter__(self):
        locked = self._lock.acquire(timeout=self._timeout)
        lock_typename = "thread" if isinstance(self._lock, _thread.LockType) else "process"
        if not locked:
            if self._timeout > 0:
                raise LockTimeout(self._timeout, lock_typename)
            else:
                raise LockHeld(f"The lock is held by another {lock_typename}.")

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._lock.release()
