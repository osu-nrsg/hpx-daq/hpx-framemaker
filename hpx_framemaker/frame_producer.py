"""frame_producer.py

FrameProducer class and subclasses

"""
import multiprocessing
import pickle
import queue
import threading
from abc import ABC, abstractmethod
from multiprocessing.queues import Queue as MPQueue
from multiprocessing.synchronize import Event as MPEvent
from typing import Any, List, Optional, Tuple, Union

import zmq
from hpx_messages import RayMsg
from zmq_quicklink import QuickSocket
from zmq_quicklink import QuickSocketContext as ZmqContext

from .fmconfig import FMConfig, angle_in_range
from .frame import Frame, SharedFrameArgs
from .timed_lock import LockHeld

from .errors import FrameError, FrameMakerError  # isort:skip  # needs to be last


ConcurrencyType = Union[threading.Thread, multiprocessing.Process]
QueueT = Union[queue.Queue, MPQueue]
EventT = Union[threading.Event, MPEvent]
ExcInfoT = Tuple[Exception, str]


class FrameProducer(ABC):
    """FrameProducer is a separate thread or process that recieves rays from the HPx Radar Server,
    builds frames, and buffers them such that a consumer may process previous frames while the current
    frame is being built.

    FrameProducer is an abstract class with concrete thread and multiprocessing process versions in
    FrameProducerTD and FrameProducerMP, respectively.
    """

    def __init__(
        self,
        config: FMConfig,
        ready_q: QueueT,
        estop: EventT,
        exc_q: QueueT,
        *,
        framebuf: Optional[List[Frame]] = None,
        frame_args: Optional[List[SharedFrameArgs]] = None,
    ):
        """Create a FrameProducer.

        See concrete classes for parameter information.
        """
        super().__init__()
        self.config: FMConfig = config
        self._multiproc = frame_args is not None
        self._estop = estop
        self._exc_q = exc_q
        self._ready_q = ready_q
        self._frame_args = frame_args
        self._framebuf = framebuf
        self._frame_i: int = -1

    @abstractmethod
    def is_alive(self):
        pass

    @abstractmethod
    def start(self):
        pass

    @abstractmethod
    def join(self):
        pass

    def stop(self):
        if self.is_alive():
            self._estop.set()

    def run(self):
        """Thread/Process target.

        Wraps the main function of the thread/process to handle any exceptions from that function.
        """
        try:
            self._create_frames()
        except BaseException as exc:  # noqa
            self._exc_q.put(pickle.dumps(exc))

    def _create_frames(self):
        """Receive rays from the 0mq endpoint and create frames."""
        # loop state vars
        ray = RayMsg.preallocate(self.config.server_nsamples)
        last_ray_azival = None

        # Get the framebuf
        framebuf: List[Frame] = []
        if self._multiproc:
            # if multiproc, set up using the Frames using the provided shared memory
            for i in range(len(self._frame_args)):
                framebuf.append(Frame(*self._frame_args[i][0], frame_sync_args=self._frame_args[i][1]))
        else:
            # if threading, just use the provided framebuf.
            framebuf = self._framebuf

        ray_sub: QuickSocket = ZmqContext.instance().socket(zmq.SUB)  # noqa  # zmq.SUB
        ray_sub.setsockopt(zmq.RCVTIMEO, 1)  # Minimize loop period
        ray_sub.subscribe("RAY")  # Make HWM max size of framebuf
        ray_sub.set_hwm(len(framebuf) * framebuf[0].max_nrays)
        with ray_sub.connected(self.config.endpoint):
            while not self._estop.is_set():
                if not ray.update_fromsocket(ray_sub):
                    # No new ray. Try again.
                    continue
                if last_ray_azival is not None:
                    self._handle_ray(ray, last_ray_azival, framebuf)
                last_ray_azival = ray.azimuth

    def _handle_ray(self, ray: RayMsg, last_ray_azival: int, framebuf: List[Frame]):
        """Check a received ray and add it to a frame if it is to be kept. Also push the frame onto the ready
        queue if it is full.

        Parameters
        ----------
        ray
            The ray to be handled
        last_ray_azival
            The azimuth value (ACP count) from the previously handled ray
        framebuf
            The buffer to which frames are written--mirrors the frame buffer in FrameMaker.

        self._frame_i is the index of the current frame being built
        """
        # Check the ray azimuth and determine if this is a new frame
        ray_azi_in_bounds, new_frame = self._check_ray_azi(ray, last_ray_azival)

        # If the azimuth is outside the bounds we're keeping, do nothing else.
        if not ray_azi_in_bounds:
            return
        # Do nothing until the first frame boundary (start_azi) is reached.
        if self._frame_i == -1 and not new_frame:
            return

        # Verification in FMConfig should prevent getting filtered rays, but throw out any we happen to get.
        # (This might happen at the azimuth boundary edges.)
        if ray.filtered:
            return

        # Is the current ray the start of a new frame?
        if new_frame:
            # Have we already filed a frame?
            if self._frame_i > -1:
                # If so, we can signal to the consumer that the full frame is ready to process.
                qpush(self._ready_q, self._frame_i)
            # update the frame index
            self._frame_i = (self._frame_i + 1) % len(framebuf)
            # Reset the now-current frame
            try:
                framebuf[self._frame_i].reset()
            except LockHeld as lock_err:
                raise FrameMakerError(
                    "The FrameMaker cannot reset the next frame in the buffer as there is currently a lock"
                    " on that frame from a reader thread/process. Speed up the reader or increase"
                    " framebuf_size in the config."
                ) from lock_err

        # If we're only collecting up until srays_per_frame and the current frame has reached that limit,
        # then don't add any more rays to the frame (wait for the next frame to start)
        if (
            self.config.frameend_criterion == "num_srays"
            and framebuf[self._frame_i].nrays >= self.config.srays_per_frame
        ):
            return

        # By reaching here all early return criteria have been overcome
        try:
            framebuf[self._frame_i].add_ray(ray)
        except LockHeld as lock_err:
            raise FrameMakerError(
                "The FrameMaker cannot add rays to a new frame as there is currently a lock on that frame"
                " from a reader thread/process. Speed up the reader or increase framebuf_size in the config."
            ) from lock_err
        except FrameError as fr_err:
            raise FrameMakerError(
                "The Frame ran out of room for new rays. Either increase the frame_max_nrays_factor or"
                " ensure that the radar is still rotating."
            ) from fr_err

    def _check_ray_azi(self, ray: RayMsg, last_ray_azival: Optional[int] = None) -> Tuple[bool, bool]:
        """Check if a ray is in the azi bounds and if it starts a new rotation

        Parameters
        ----------
        ray
            The current RayMsg.
        last_ray_azival
            The ACP count from the last ray. None if this is the first ray.

        Returns
        -------
        tuple
            - azi_in_bounds: The ray is within the desired azimuth bounds
            - new_frame: This ray is the first in a new frame
        """
        # Check that the current ray's azimuth is within the azimuth bounds
        ray_azid = ray.azimuth * 360.0 / 2 ** 16
        azi_in_bounds = (self.config.end_azi is None) or angle_in_range(
            ray_azid, self.config.start_azi, self.config.end_azi
        )

        # Check for new frame by comparing the current ray azi and the last ray azi relative to start_azi
        azid0 = (ray_azid - self.config.start_azi) % 360.0
        last_azid = last_ray_azival * 360.0 / 2 ** 16
        last_azid0 = (last_azid - self.config.start_azi) % 360.0
        new_frame = azid0 < last_azid0

        return azi_in_bounds, new_frame


class FrameProducerTD(FrameProducer, threading.Thread):
    """Thread concrete subclass of FrameProducer"""

    def __init__(
        self,
        config: FMConfig,
        ready_q: queue.Queue,
        estop: threading.Event,
        exc_q: queue.Queue,
        framebuf: List[Frame],
    ):
        """Create a FrameProducerTD.

        Parameters
        ----------
        config
            A frame maker configuration object.
        ready_q
            Queue to hold the indexes of frames ready to be processed
        estop
            Event object to signal FrameProducerTD to stop
        exc_q
            Queue to capture exceptions from FrameProducerTD
        framebuf
            The buffer of frames to use for building and retrieving rays
        """
        threading.Thread.__init__(self, daemon=True)
        FrameProducer.__init__(self, config, ready_q, estop, exc_q, framebuf=framebuf)

    def start(self):
        threading.Thread.start(self)

    def join(self):
        threading.Thread.join(self)

    def is_alive(self):
        return threading.Thread.is_alive(self)


class FrameProducerMP(FrameProducer, multiprocessing.Process):
    """Multiprocessing.Process concrete subclass of FrameProducer"""

    def __init__(
        self,
        config: FMConfig,
        ready_q: MPQueue,
        estop: MPEvent,
        exc_q: MPQueue,
        frame_args: List[SharedFrameArgs],
    ):
        """Create a FrameProducerMP.

        Parameters
        ----------
        config
            A frame maker configuration object.
        ready_q
            Queue to hold the indexes of frames ready to be processed
        estop
            Event object to signal FrameProducerMP to stop
        exc_q
            Queue to capture exceptions from FrameProducerMP
        frame_args
            Information to initialize and synchronize an internal frame buffer instance that mirrors a frame
            buffer in the consumer using shared memory.
        """
        multiprocessing.Process.__init__(self, daemon=True)
        FrameProducer.__init__(self, config, ready_q, estop, exc_q, frame_args=frame_args)

    def start(self):
        multiprocessing.Process.start(self)

    def join(self):
        multiprocessing.Process.join(self)

    def is_alive(self):
        return multiprocessing.Process.is_alive(self)


# #######################
# ## Utility functions ##
# #######################


def qpush(q: QueueT, item: Any):
    """Continue to try to put an item on a queue, removing items from the back as necessary."""
    while True:
        try:
            q.put_nowait(item)
        except queue.Full:
            try:
                q.get_nowait()
            except queue.Empty():
                pass
        else:
            break


def qget(q: QueueT, default: Any):
    """Try to get an item from a queue and return `default` if the queue is empty."""
    try:
        return q.get_nowait()
    except queue.Empty:
        return default
