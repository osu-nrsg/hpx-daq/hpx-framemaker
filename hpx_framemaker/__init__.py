from pathlib import Path as _Path

from single_version import get_version as _get_version

from . import fmconfig as _fmconfig
from .errors import FMConfigError, FrameError, FrameMakerError
from .frame import Frame
from .frame_maker import FMConfig, FrameMaker

__version__ = _get_version(__name__, _Path(__file__).parent.parent)
