"""
FrameMaker class. See FrameMaker class docs for more info.
"""
from __future__ import annotations

import logging
import multiprocessing
import pickle
import queue
import threading
from contextlib import contextmanager
from multiprocessing.managers import SharedMemoryManager
from typing import Generator, List, Optional

import numpy as np

from .fmconfig import FMConfig
from .frame import Frame
from .frame_producer import EventT, FrameProducer, FrameProducerMP, FrameProducerTD, QueueT

logger = logging.getLogger(__name__)


class FrameMaker:
    """A FrameMaker receives radar data from an HPx Radar Server and generates frames (sweeps segments) for
    downstream processing.
    """

    def __init__(self, config: FMConfig, *, multiproc: bool = False):
        """Set up a FrameMaker instance.

        Parameters
        ----------
        config
            FMConfig instance containing the pre-checked configuration parameters for the FrameMaker.
        multiproc
            Enable frame acquisition in a separate process, rather than a separate thread.

        Raises
        ------
        ValueError
            _framebuf_size is less than 2.
        """
        # Initialize attrs
        self.config: FMConfig = config
        self._multiproc: bool = multiproc
        self._frame_i: int = -1
        self._nframes: int = config.framebuf_size  # number of frames in _framebuf
        self._framebuf: List[Frame] = []

        # Multiprocessing and threading use different event and queue types, and multiprocessing uses
        # shared memory
        if multiproc:
            self._estop: EventT = multiprocessing.Event()
            self._ready_frame_q: QueueT = multiprocessing.Queue(self._nframes)
            self._exc_q: QueueT = multiprocessing.Queue(1)
            self._memmgr: Optional[SharedMemoryManager] = SharedMemoryManager()
            self._memmgr.start()
        else:
            self._estop: EventT = threading.Event()
            self._ready_frame_q: QueueT = queue.Queue(self._nframes)
            self._exc_q: QueueT = queue.Queue(1)
            self._memmgr: Optional[SharedMemoryManager] = None

        # The frame buffer allows for processing one full frame while the next frame is being built
        for _ in range(self._nframes):
            self._framebuf.append(
                Frame(
                    int(config.srays_last_sweep * config.frame_max_nrays_factor),
                    config.nsamples,
                    config.rg_slice,
                    shared_mem_mgr=self._memmgr,
                )
            )
        if self._multiproc:
            # For multiprocessing we share the shared memory objects from the frames in the frame buffer with
            # the FrameProducer through the shared_frame_args
            frame_args = [frame.shared_frame_args for frame in self._framebuf]
            self._producer: FrameProducer = FrameProducerMP(
                self.config, self._ready_frame_q, self._estop, self._exc_q, frame_args=frame_args
            )
        else:
            # For the threaded FrameProducer we can just share the frame buffer
            self._producer: FrameProducer = FrameProducerTD(
                self.config, self._ready_frame_q, self._estop, self._exc_q, framebuf=self._framebuf
            )

    # Context manager methods for using FrameMaker in a `with` block
    def __enter__(self):
        self.start()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()

    # Properties to access internal variables
    @property
    def endpoint(self) -> str:
        return self.config.endpoint

    @property
    def nsamples(self) -> int:
        return self.config.nsamples

    @property
    def rg(self) -> np.ndarray:
        return self.config.rg

    # FrameMaker itself doesn't run as a separate thread or process, but the enclosed FrameProducer is.
    # The below methods access the typical thread/process methods
    def start(self):
        """Initialize and start the FrameProducer process or thread"""
        self._producer.start()

    def join(self):
        """Join the FrameProducer process or thread."""
        self._producer.join()

    def is_alive(self) -> bool:
        """Determine if FrameProducer thread or process is alive."""
        return self._producer.is_alive()

    def stop(self):
        """Stop and join the thread/process."""
        if self.is_alive():
            self._estop.set()
        self.join()
        if self._memmgr:
            self._memmgr.shutdown()

    def reraise_producer_exceptions(self):
        """Raise exceptions from the FrameMaker thread or process.

        This should be run for any actions that interact with frames.
        """
        try:
            e = self._exc_q.get_nowait()
        except queue.Empty:
            pass
        else:
            raise pickle.loads(e)

    @contextmanager
    def next_frame(self, timeout: Optional[float] = None) -> Optional[Generator[Frame, None, None]]:
        """Context manager for getting and locking access to the data from a frame.

        This will raise any exceptions from the worker thread/process each time it is called.

        Parameters
        ----------
        timeout
            Max seconds to wait for a new frame. Default is None (infinite). If no frame is available
            before the timeout, None is returned.
        """
        self.reraise_producer_exceptions()
        try:
            frame_i = self._ready_frame_q.get(timeout=timeout)
            with self._framebuf[frame_i].lock:
                yield self._framebuf[frame_i]
        except queue.Empty:
            yield None
